/**
 * Copyright (c) 2014 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
// Imports are listed in full to show what's being used
// could just import javax.swing.* and java.awt.* etc..

package cern.cbng.test.client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;

public class TestGui {

    // Note: Typically the main method will be in a
    // separate class. As this is a simple one class
    // example it's all in the one class.
    @SuppressWarnings("unused")
    public static void main(String[] args) {
        new TestGui();
    }

    public TestGui() {
        JFrame guiFrame = new JFrame();

        // make sure the program exits when the frame closes
        guiFrame.getContentPane().setBackground(new Color(59, 203, 203));
        guiFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        guiFrame.setTitle("Example GUI");
        guiFrame.setSize(500, 400);

        // This will center the JFrame in the middle of the screen
        guiFrame.setLocationRelativeTo(null);

        // Options for the JComboBox
        String[] appOptions = { "Jnlp", "Service" };

        // Options for the JList
        String[] buildOptions = { "Gradle", "Ant" };

        // The first JPanel contains a JLabel and JCombobox
        final JPanel comboPanel = new JPanel();
        JLabel comboLbl = new JLabel("Application:");
        JComboBox<String> app = new JComboBox<>(appOptions);

        comboPanel.add(comboLbl);
        comboPanel.add(app);

        // Create the second JPanel. Add a JLabel and JList and
        // make use the JPanel is not visible.
        final JPanel listPanel = new JPanel();
        listPanel.setBackground(new Color(153, 223, 39));
        listPanel.setVisible(false);
        JLabel listLbl = new JLabel("Build system:");
        JList<String> bld = new JList<>(buildOptions);
        bld.setLayoutOrientation(JList.HORIZONTAL_WRAP);

        listPanel.add(listLbl);
        listPanel.add(bld);

        JButton test = new JButton("TEST");
        test.setBorderPainted(true);
        test.setSize(20, 15);
        test.setBackground(Color.BLUE);
        test.setForeground(Color.WHITE);

        // The ActionListener class is used to handle the
        // event that happens when the user clicks the button.
        // As there is not a lot that needs to happen we can
        // define an anonymous inner class to make the code simpler.
        test.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                // When the test button is pressed
                // the setVisible value of the listPanel and
                // comboPanel is switched from true to
                // value or vice versa.
                listPanel.setVisible(!listPanel.isVisible());
                comboPanel.setVisible(!comboPanel.isVisible());

            }
        });

        // The JFrame uses the BorderLayout layout manager.
        // Put the two JPanels and JButton in different areas.
        guiFrame.add(comboPanel, BorderLayout.NORTH);
        guiFrame.add(listPanel, BorderLayout.CENTER);
        guiFrame.add(test, BorderLayout.SOUTH);

        // make sure the JFrame is visible
        guiFrame.setVisible(true);
    }

}
