/**
 * Copyright (c) 2013 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.cbng.test.client;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.Socket;

/**
 * <code>Client</code> is the test application to test deployment
 * 
 * @author Natalia Yastrebova
 */
public class Client {
    public static boolean dummy(int i) {
        if (i > 0) {
            return true;
        } else if (i < 0) {
            return false;
        } else {
            throw new RuntimeException();
        }
    }

    /**
     * @param args
     */
    public static void main(String[] args) throws Exception {
        for (int i = 0; i < 100; ++i) {
            Socket client = new Socket("localhost", 5556);
            DataOutputStream out = new DataOutputStream(client.getOutputStream());
            BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));
            String message = "test\n";
            out.writeBytes(message);
            String reply = in.readLine();
            System.out.println("FROM SERVER: " + reply);
            client.close();
            Thread.sleep(1000);
        }
    }
   
}
