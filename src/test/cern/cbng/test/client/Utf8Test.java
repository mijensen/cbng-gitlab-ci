/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.cbng.test.client;

import static java.nio.charset.StandardCharsets.UTF_8;

import org.junit.Assert;
import org.junit.Test;

public class Utf8Test {
    private static final String STR1 = "áéó űő ŰŐ";
    private static final String STR2 = "\u00E1\u00E9\u00F3 \u0171\u0151 \u0170\u0150";

    private static String sut(String str) {
        return new String(str.getBytes(UTF_8), UTF_8);
    }

    @Test
    public void test0() {
        Assert.assertEquals(STR1, STR2);
    }

    @Test
    public void test1() {
        Assert.assertEquals(STR1, sut(STR1));
    }

    @Test
    public void test2() {
        Assert.assertEquals(STR2, sut(STR2));
    }

    @Test
    public void test3() {
        Assert.assertEquals(STR1, sut(STR2));
    }

    @Test
    public void test4() {
        Assert.assertEquals(STR2, sut(STR1));
    }

    @Test
    public void test5() {
        Assert.assertEquals(sut(STR1), sut(STR2));
    }

}
