/**
 * Copyright (c) 2016European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.cbng.test.client;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ClientTest {
    @Test
    public void testDummyTrue() {
        assertTrue(Client.dummy(1));
    }

    @Test
    public void testDummyFalse() {
        assertFalse(Client.dummy(-1));
    }

    @Test(expected = RuntimeException.class)
    public void testDummyException() {
        Client.dummy(0);
    }

    @Test
    public void testFailure() {
        assertFalse(System.getenv("FAIL_TEST").equalsIgnoreCase("true"));
    }
}
